package com.hipacy.texter2.controller;


import com.hipacy.texter2.model.Post;
import com.hipacy.texter2.model.User;
import com.hipacy.texter2.service.PostService;
import com.hipacy.texter2.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Locale;

@Controller
public class PostController {


    @Autowired
    PostService postService;

    @Autowired
    UserService userService;



    @RequestMapping(value = {"/", "/post"}, method = RequestMethod.GET)
    public String getPosts(ModelMap model){
        Post post = new Post();

        List<Post> posts = postService.findAllPosts();


        model.addAttribute("posts", posts);
        model.addAttribute("post", post);
        //model.addAttribute("loggedinuser", userService.findBySSO(userService.getPrincipal()));
        ///post.setUser(userService.findBySSO(userService.getPrincipal()));
        return "main";
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.POST)
    public String savePost(@ModelAttribute("post") Post post, BindingResult result,
                           ModelMap model) {
        LocalDateTime localDateTime = LocalDateTime.now();

        if (result.hasErrors()) {
            return "redirect:/";
        }
        System.out.println(post.toString() + " 1");
        System.out.println(userService.getPrincipal());
        post.setApp_user(userService.findBySSO(userService.getPrincipal()));
        post.setDateTime(localDateTime);
        System.out.println(
                post.getUser().getSsoId() + post.getUser().getEmail()
        );
        postService.savePost(post);
        //return "success";
        return "redirect:/";
    }


    @RequestMapping(value = { "/post/delete-post-{id}" }, method = RequestMethod.GET)
    public String deletePost(@PathVariable Integer id) {
        postService.deletePost(id);
        return "redirect:/post";
    }


    @PreAuthorize("hasRole('DBA') or hasRole('ADMIN')")
    @RequestMapping(value = { "/post/edit-post-{id}" }, method = RequestMethod.GET)
    public String updatePost(@PathVariable Integer id, ModelMap model) {
        Post post = postService.findById(id);
        boolean edit = false;
        model.addAttribute("post", post);
        model.addAttribute("edit", edit);
        //model.addAttribute("edit", true);
        //model.addAttribute("loggedinuser", userService.getPrincipal());
        return "editpost";
    }

    /**
     * This method will be called on form submission, handling POST request for
     * updating user in database. It also validates the user input
     */
    @PreAuthorize("hasRole('DBA') or hasRole('ADMIN')")
    @RequestMapping(value = { "/post/edit-post-{id}" }, method = {RequestMethod.POST})
    public String updatePost(@PathVariable Integer id, @ModelAttribute("post") Post post, BindingResult result,
                             ModelMap model) {

        if (result.hasErrors()) {

            return "main";
        }
        System.out.println(post.getUser() + " " + post.getTitle());

        post.setApp_user(userService.findBySSO(userService.getPrincipal()));
        post.setDateTime(LocalDateTime.now());
        postService.updatePost(post);


        // model.addAttribute("loggedinuser", userService.getPrincipal());
        return "editpostsuccess";
    }

        @RequestMapping(value = {"/search"}, method = RequestMethod.POST)
        public String searchCustomers(@RequestParam("theSearchName") String theSearchName, ModelMap model) {
            Post post = new Post();
            String textt = "Search result:";
            // search customers from the service
            List<Post> posts = postService.findAPost(theSearchName);

            // add the customers to the model
            model.addAttribute("posts", posts);
            model.addAttribute("post", post);
            model.addAttribute("textt", textt);



            return "main";
        }
    }



