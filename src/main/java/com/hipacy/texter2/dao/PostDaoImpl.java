package com.hipacy.texter2.dao;

import com.hipacy.texter2.model.Post;

import org.hibernate.Criteria;

import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository("postDao")
public class PostDaoImpl extends AbstractDao<Integer, Post> implements  PostDao {


    @Override
    public Post findById(int id) {
        Post post = getByKey(id);
        return post;
    }

    @Override
    public void save(Post post) {
       saveOrUpdate(post);
        //persist(post);
    }

    @Override
    public void deleteById(Integer id) {

        delete(findById(id));
    }

    @Override
    public List<Post> findAllPosts() {
        Criteria criteria = createEntityCriteria().addOrder(Order.asc("dateTime"));
        criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);//To avoid duplicates.
        List<Post> posts = (List<Post>) criteria.list();
        return posts;


    }

    @Override
    public List<Post> findAPost(String search) {
        Criteria crit = createEntityCriteria();
        crit.add(Restrictions.like("title", "%"+search+"%"));

        return (List<Post>)crit.list();
    }


}
