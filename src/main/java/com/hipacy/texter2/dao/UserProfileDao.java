package com.hipacy.texter2.dao;

import java.util.List;

import com.hipacy.texter2.model.UserProfile;


public interface UserProfileDao {

	List<UserProfile> findAll();
	
	UserProfile findByType(String type);
	
	UserProfile findById(int id);
}
