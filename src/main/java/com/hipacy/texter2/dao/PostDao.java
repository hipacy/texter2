package com.hipacy.texter2.dao;

import com.hipacy.texter2.model.Post;


import java.util.List;

public interface PostDao {


    Post findById(int id);

    void save(Post post);

    void deleteById(Integer id);

    List<Post> findAllPosts();

    List<Post> findAPost(String search);

}
