package com.hipacy.texter2.service;

import java.util.List;

import com.hipacy.texter2.model.UserProfile;


public interface UserProfileService {

	UserProfile findById(int id);

	UserProfile findByType(String type);
	
	List<UserProfile> findAll();
	
}
