package com.hipacy.texter2.service;

import com.hipacy.texter2.model.Post;

import java.util.List;


public interface PostService {

    Post findById(int id);

    void savePost(Post post);

    void updatePost(Post post);

    void deletePost(Integer id);

    List<Post> findAllPosts();

    List<Post> findAPost(String search);
}
