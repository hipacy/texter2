package com.hipacy.texter2.service;

import com.hipacy.texter2.dao.PostDao;
import com.hipacy.texter2.model.Post;
import com.hipacy.texter2.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService {

    @Autowired
    PostDao dao;





    @Override
    public Post findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void savePost(Post post) {
        dao.save(post);

    }

    @Override
    public void updatePost(Post post) {
        Post entity = dao.findById(post.getId());

        if(entity!=null){
            entity.setId(post.getId());
            entity.setTitle(post.getTitle());
            entity.setText(post.getText());
            entity.setDateTime(post.getDateTime());
            entity.setUser(post.getUser());

        }
    }

    @Override
    public void deletePost(Integer id) {
        dao.deleteById(id);

    }

    @Override
    public List<Post> findAllPosts() {
        return dao.findAllPosts();
    }

    @Override
    public List<Post> findAPost(String search) {
        return dao.findAPost(search);
    }
}
