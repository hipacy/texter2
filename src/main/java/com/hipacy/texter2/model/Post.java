package com.hipacy.texter2.model;


import org.hibernate.validator.constraints.Length;


import javax.persistence.*;

import java.time.LocalDateTime;

import com.hipacy.texter2.converter.LocalDateTimePersistenceConverter;
import org.hibernate.validator.constraints.NotEmpty;


@Entity
@Table(name = "post")
public class Post {



    @Id
    @GeneratedValue
    private int Id;



    @Length(min = 5, message = "*Your title must have at least 5 characters")
    @NotEmpty
    private String title;


    @Length(min = 5, message = "*Your text must have at least 50 characters")
    @NotEmpty
    private String text;

    @Column(nullable = false)
    @Convert(converter = LocalDateTimePersistenceConverter.class)
    private LocalDateTime dateTime;


    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
            CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "user_id", nullable = false, referencedColumnName = "id")
    private User app_user;



    public Post(){

    }
    public Post(String title, String text, LocalDateTime dateTime, User user) {
        this.title = title;
        this.text = text;
        this.dateTime = dateTime;
        this.app_user = user;
    }


    public void setApp_user(User user) {
        setApp_user(user, true);
    }

    void setApp_user(User user, boolean add) {
        this.app_user = user;
        if (user != null && add) {
            user.addPost(this, false);
        }
    }



    public Post(String title) {
        this.title = title;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public User getUser() {
        return app_user;
    }



    public void setUser(User user) {
        this.app_user = user;
    }



        @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (Id != post.Id) return false;
        if (title != null ? !title.equals(post.title) : post.title != null) return false;
        if (text != null ? !text.equals(post.text) : post.text != null) return false;
        if (dateTime != null ? !dateTime.equals(post.dateTime) : post.dateTime != null) return false;
        return app_user != null ? app_user.equals(post.app_user) : post.app_user == null;
    }

    @Override
    public int hashCode() {
        int result = Id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (dateTime != null ? dateTime.hashCode() : 0);
        result = 31 * result + (app_user != null ? app_user.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Post{" +
                "Id=" + Id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", dateTime=" + dateTime +
                ", app_user=" + app_user +
                '}';
    }
}
