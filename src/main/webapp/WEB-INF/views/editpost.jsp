<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>
<head>
    <title>The Texter2</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<c:url value='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' />"
          rel="stylesheet"/>

    <style>
        .footer {
            position: -webkit-sticky; /* Safari */
            position: sticky;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 60px;
            line-height: 60px; /* Vertically center the text there */
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
<%@include file="authheader.jsp" %>
<br>


<sec:authorize access="hasRole('ADMIN')">

    <div class="container">
               <div class="card my-4">
            <h5 class="card-header">Add a post:</h5>
            <div class="card-body">
                <div class="form-group col-md-12">
                    <form:form method="POST" modelAttribute="post" class="form-horizontal">
                    <form:input type="hidden" path="id" id="${post.id}"/>



                    <div class="form-group col-md-12">
                        <label class="col-md-3 control-lable" for="title">Title</label>
                        <div class="col-md-11">
                            <form:input type="text" path="title" id="title" class="form-control input-sm" />
                        </div>
                    </div>

                    <div class="col-md-11 form-group">
                        <label class="col-md-3 control-label" for="text">Content</label>
                        <form:textarea path="text" id="text"  class="form-control" rows="3"/>
                    </div>

                        <input type="submit" value="Update" class="btn btn-primary btn-sm"/>


                </div>
                </form:form>

            </div>

        </div>
    </div>
</sec:authorize>

<footer class="footer">
    <div class="container">
        <span class="text-muted">Copyright M. Rakieta</span>
    </div>
</footer>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>


</body>
</html>
