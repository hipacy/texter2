	<div class="container">
		<span>Dear <strong>${loggedinuser}</strong>, Welcome to the Texter2.</span> <span class="floatRight"><a href="<c:url value="/logout" />">Log me out of here.</a></span>
	</div>
	<br>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
		<div class="container">
			<a class="navbar-brand" href="/"><span class="h3">The Texter</span><span class="h2">2</span></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a class="nav-link h5" href="/">Home</a>
					</li>

					<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
						<li class="nav-item">
							<a class="nav-link h5" href="/user/newuser">New user</a>
						</li>
					</sec:authorize>
					<sec:authorize access="hasRole('ADMIN') or hasRole('DBA')">
						<li class="nav-item">
							<a class="nav-link h5" href="/list">The List of Users</a>
						</li>
					</sec:authorize>
					<sec:authorize access="hasRole('ADMIN') or hasRole('DBA') or hasRole('USER')">
						<li class="nav-item">
							<a class="nav-link h6 default-color6" href="/logout">logout</a>
						</li>
					</sec:authorize>
				</ul>
			</div>
		</div>
	</nav>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
